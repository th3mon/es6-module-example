require.config({
	baseUrl: 'dist'
});

require(['my_library'], function(MyLibrary) {
	MyLibrary = MyLibrary || window.MyLibrary;

    MyLibrary.test();
});