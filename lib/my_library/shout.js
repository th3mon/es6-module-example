// jshint esnext:true
var shout = function(s) {
	return s.toUpperCase();
};

export { shout };